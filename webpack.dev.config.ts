import * as CopyWebpackPlugin from 'copy-webpack-plugin'
import * as ExtractTextPlugin from 'extract-text-webpack-plugin';
import * as ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import * as HappyPack from 'happypack';
import * as HardSourceWebpackPlugin from 'hard-source-webpack-plugin';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import * as path from 'path';
import * as webpack from 'webpack';
// import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

const config: webpack.Configuration = {
    entry: path.join(__dirname, 'src/app.ts'),
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'game.js'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.css', '.scss'],
        modules: [
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, 'lib')
        ],
        alias: {
            'pixi': path.join(__dirname, 'node_modules/phaser-ce/build/custom/pixi.js'),
            'phaser-ce': path.join(__dirname, 'node_modules/phaser-ce/build/custom/phaser-split.js'),
            'p2': path.join(__dirname, 'node_modules/phaser-ce/build/custom/p2.js'),
            'assets': path.join(__dirname, 'assets/'),
            'photon': path.join(__dirname, 'lib/Photon/Photon-Javascript_SDK.js'),
            'phaser-spine': path.join(__dirname, 'node_modules/@orange-games/phaser-spine/build/phaser-spine.js')
        }
    },
    plugins: [
        new HardSourceWebpackPlugin(),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true,
            tslint: true,
            watch: ['./src'] // optional but improves performance (less stat calls)
        }),
        new HappyPack({
            id: 'ts',
            loaders: [
                {
                    path: 'babel-loader',
                    query: {}
                },
                {
                    path: 'ts-loader',
                    query: { happyPackMode: true }
                }
            ]
        }),
        new webpack.ProvidePlugin({
            FastClick: 'fastclick'
        }),
        new webpack.DefinePlugin({
            DEBUG: JSON.stringify(true),
            UAT: JSON.stringify(true),
        }),
        new HtmlWebpackPlugin({
            version: JSON.stringify(require("./package.json").version),
            template: path.join(__dirname, 'templates/index.ejs'),
            build: "d",
        }),
        new CopyWebpackPlugin([{
            from: 'assets',
            to: 'assets'
        }]),
        new ExtractTextPlugin('bundle.css'),
        // new BundleAnalyzerPlugin(
        //     {
        //         analyzerMode: 'server',
        //         analyzerHost: '127.0.0.1',
        //         analyzerPort: 8889,
        //         reportFilename: 'report.html',
        //         defaultSizes: 'parsed',
        //         openAnalyzer: true,
        //         generateStatsFile: false,
        //         statsFilename: 'stats.json',
        //         statsOptions: null,
        //         logLevel: 'info'
        //     }
        // ),
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        host: '0.0.0.0',
        port: 7002,
        inline: true,
        watchOptions: {
            aggregateTimeout: 300,
            poll: true,
            ignored: /node_modules/
        },
        // public: '192.168.205.57'
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'happypack/loader?id=ts',
                exclude: '/node_modules/'
            },
            // {
            //     test: /\.tsx$/,
            //     use: ['ts-loader'],
            //     exclude: '/node_modules/'
            // },
            // {
            //     test: /\.ts$/,
            //     enforce: 'pre',
            //     use: 'tslint-loader',
            //     exclude: '/node_modules/'
            // },
            {
                test: /assets(\/|\\)/,
                use: 'file-loader?name=assets/[hash].[ext]'
            },
            {
                test: /phaser\-spine\.js$/,
                use: 'exports-loader?PhaserSpine=true'
            },
            {
                test: /pixi\.js$/,
                use: 'expose-loader?PIXI'
            },
            {
                test: /phaser-split\.js$/,
                use: 'expose-loader?Phaser'
            },
            {
                test: /p2\.js$/,
                use: 'expose-loader?p2'
            },
            {
                test: /\.(sass|scss)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'resolve-url-loader', 'sass-loader']
                }),
            }
        ]
    },
    devtool: 'eval'
};

export default config;
