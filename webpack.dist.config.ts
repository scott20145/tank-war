import * as CleanWebpackPlugin from 'clean-webpack-plugin';
import * as CopyWebpackPlugin from 'copy-webpack-plugin'
import * as ExtractTextPlugin from 'extract-text-webpack-plugin';
import * as ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import * as HappyPack from 'happypack';
import * as HardSourceWebpackPlugin from 'hard-source-webpack-plugin';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import * as os from 'os';
import * as path from 'path';
import * as UglifyJsParallelPlugin from 'uglifyjs-webpack-plugin';
import * as uuid from 'uuid';
import * as webpack from 'webpack';

const isProd = !!process.env.isProd;
const stripTableFileName = `strip_table${uuid()}.json`;

const config: webpack.Configuration = {
    entry: path.join(__dirname, 'src/app.ts'),
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'game[hash].min.js'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.css', '.scss'],
        modules: [
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, 'lib')
        ],
        alias: {
            'pixi': path.join(__dirname, 'node_modules/phaser-ce/build/custom/pixi.js'),
            'phaser-ce': path.join(__dirname, 'node_modules/phaser-ce/build/custom/phaser-split.js'),
            'phaser-spine': path.join(__dirname, 'node_modules/@orange-games/phaser-spine/build/phaser-spine.js'),
            'p2': path.join(__dirname, 'node_modules/phaser-ce/build/custom/p2.js'),
            'assets': path.join(__dirname, 'assets/'),
            'photon': path.join(__dirname, 'lib/Photon/Photon-Javascript_SDK.js')
        }
    },
    plugins: [
        new HardSourceWebpackPlugin(),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true,
            tslint: true,
            watch: ['./src'] // optional but improves performance (less stat calls)
        }),
        new HappyPack({
            id: 'ts',
            loaders: [
                {
                    path: 'babel-loader',
                    query: {}
                },
                {
                    path: 'ts-loader',
                    query: { happyPackMode: true }
                }
            ]
        }),
        new webpack.ProvidePlugin({
            FastClick: 'fastclick'
        }),
        new webpack.DefinePlugin({
            'DEBUG': JSON.stringify(false),
            'UAT': JSON.stringify(false),
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new CleanWebpackPlugin([
            path.join(__dirname, 'dist')
        ]),
        new UglifyJsParallelPlugin({
            parallel: true,
            uglifyOptions: {
                warnings: false,
                mangle: true,
                compress: {
                    drop_console: true,
                    drop_debugger: true
                },
                output: {
                    comments: false
                }
            },
        }),
        new HtmlWebpackPlugin({
            version: JSON.stringify(require("./package.json").version),
            stripTableFileName,
            template: path.join(__dirname, 'templates/index-prod.ejs'),
            build: "p",
        }),
        new ExtractTextPlugin('bundle.css')
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    { loader: 'babel-loader' },
                    { loader: 'happypack/loader?id=ts' }
                ],
                exclude: '/node_modules/'
            },
            {
                test: /assets(\/|\\)/,
                use: 'file-loader?name=assets/[hash].[ext]'
            },
            {
                test: /pixi\.js$/,
                use: 'expose-loader?PIXI'
            },
            {
                test: /phaser\-spine\.js$/,
                use: 'exports-loader?PhaserSpine=true'
            },
            {
                test: /phaser-split\.js$/,
                use: 'expose-loader?Phaser'
            },
            {
                test: /p2\.js$/,
                use: 'expose-loader?p2'
            },
            {
                test: /\.(sass|scss)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'resolve-url-loader', 'sass-loader'],
                }),
            }
        ]
    }
};

export default config;
