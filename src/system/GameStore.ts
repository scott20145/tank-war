
import { WorldBlock } from "component/WorldBlock";
import { PlayerInfo } from "./config";

class GameStore {
    public player: PlayerInfo;
    public createdBlocks: { [key: string]: WorldBlock } = {};
    constructor() {
        this.player = {
            tank: undefined
        }
    }
}

export const gameStore = new GameStore();
