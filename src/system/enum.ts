export enum ObstacleDef {
    HAY = 0,
    WALL
}

export enum TankDef {
    RED = 0,
    BLUE,
    GREEN
}

export enum TankTextureDef {
    RED = 'tank_red',
    BLUE = 'tank_blue',
    GREEN = 'tank_green'
}

export enum Direction {
    LEFT = 0,
    RIGHT,
    UP,
    DOWN
}
