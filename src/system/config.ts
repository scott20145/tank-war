/**
 * Game configs
 */
import { ObstacleDef, TankDef, TankTextureDef } from "./enum";

export const isDebug = true;
export const obstacleTable = [3, 10];

export interface ObstacleMapping {
    scale: number;
    hasBody: boolean;
    health?: number;
    cankill: boolean;
}

export interface TankMapping {
    texture: TankTextureDef,
    damage: number,
    speed: number,
    fireRate: number,
    bulletSpeed: number
}

export interface PlayerInfo {
    tank: TankMapping;
}

export const obstacleConfig: { [key: number]: ObstacleMapping } = {
    [ObstacleDef.HAY]: {
        scale: 0.2,
        hasBody: false,
        health: 100,
        cankill: true
    },
    [ObstacleDef.WALL]: {
        scale: 1,
        hasBody: true,
        cankill: false
    },
}

export const tankConfig: { [key: number]: TankMapping } = {
    [TankDef.RED]: {
        texture: TankTextureDef.RED,
        damage: 10,
        bulletSpeed: 400,
        fireRate: 3,
        speed: 200
    },
    [TankDef.BLUE]: {
        texture: TankTextureDef.BLUE,
        damage: 20,
        bulletSpeed: 400,
        fireRate: 3,
        speed: 200
    },
    [TankDef.GREEN]: {
        texture: TankTextureDef.GREEN,
        damage: 25,
        bulletSpeed: 400,
        fireRate: 3,
        speed: 200
    }
}
