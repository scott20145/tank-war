export interface Point {
    x: number;
    y: number;
}

export interface Bound {
    left: number;
    up: number;
    right: number;
    down: number
}

export interface MapGenerator {
    generate: Function
}
