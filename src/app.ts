import 'babel-polyfill';

import 'p2';
import 'pixi';

import { CANVAS, Game } from 'phaser-ce';
import { BootState } from 'state/BootState';
import { GameState } from 'state/GameState';
import { MenuState } from 'state/MenuState';

window.onload = () => {
    const game = new Game(1920, 1080, CANVAS, 'main');
    game.state.add('menu', MenuState);
    game.state.add('game', GameState);
    game.state.add('boot', BootState);
    // game.transparent = true;
    game.state.start('boot');
};
