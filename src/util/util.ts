import { obstacleTable } from "system/config";
import { Bound, Point } from "system/interface";

export const random = (min: number, max: number) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

export const getObstacleType = (seed: number) => {
    let idx = 0;
    while (obstacleTable[idx] < seed) {
        idx++;
    }
    return idx;
}

export const getCenterByBlockIndex = (x: number, y: number, width = 1920, height = 1080): Point => {
    return {
        x: width * x + width / 2,
        y: height * y + height / 2,
    }
}

export const getBlockIndexByPosition = (x: number, y: number, width = 1920, height = 1080): Point => {
    return {
        x: Math.floor(x / width),
        y: Math.floor(y / height)
    }
}

export const getBoundByBlockIndex = (x: number, y: number, width = 1920, height = 1080): Bound => {
    return {
        left: width * x,
        up: height * y,
        right: width * (x + 1),
        down: height * (y + 1)
    }
}
