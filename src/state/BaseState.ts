
import { State } from "phaser-ce";
import { SpinePlugin } from "phaser-spine";

export class BaseState extends State {

    init() {
        // 讓 CANVAS模式可用 mesh
        this.game.plugins.add(SpinePlugin as any, { triangleRendering: true })
    }

    shutdown() {
        this.removeEvents();

        this.game.world.removeAll(true);
    }

    removeEvents() {
    }
}
