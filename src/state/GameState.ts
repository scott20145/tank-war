
import { ControlPanel } from "component/ControlPanel";
import { Player } from "component/Player";
import { World } from "component/World";
import { Physics, State, TileSprite } from "phaser-ce";
import { isDebug } from "system/config";
import { Point } from "system/interface";
import { getCenterByBlockIndex, random } from "util/util";

export class GameState extends State {
    static player: Player;
    static world: World;
    static controlPanel: ControlPanel;

    static collisionGroup: Physics.P2.CollisionGroup;
    preload() {
        this.game.load.image('hay', 'assets/hay.png');
        this.game.load.image('wall', 'assets/wall.png');
        this.game.load.image('bullet', 'assets/bullet.png');
        this.game.load.image('tank_red', 'assets/tank_red.png');
        this.game.load.image('tank_blue', 'assets/tank_blue.png');
        this.game.load.image('tank_green', 'assets/tank_green.png');

        this.game.load.image('bg', 'assets/bg.jpg');
        this.game.load.image('grass', 'assets/grass.png');
    }
    init() {
        super.init();
        this.game.config.enableDebug = true;
    }

    create() {
        const bg = this.game.add.tileSprite(0, 0, 1920, 1080, 'grass') as TileSprite;
        bg.fixedToCamera = true;

        this.game.physics.startSystem(Phaser.Physics.P2JS);
        this.game.physics.p2.setImpactEvents(true);
        this.game.physics.p2.applyGravity = false;

        const blockIndex: Point = { x: random(300, 300), y: random(300, 300) };
        const playerPoint = getCenterByBlockIndex(blockIndex.x, blockIndex.y);

        // world layer
        GameState.world = new World(this.game, blockIndex.x, blockIndex.y);
        // player layer
        GameState.player = new Player(this.game, playerPoint.x, playerPoint.y);
        // control layer
        GameState.controlPanel = new ControlPanel(this.game, 0, this.game.height - 300);
        GameState.controlPanel.cameraOffset.setTo(0, this.game.height - 300);

        this.game.world.setBounds(0, 0, Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER);
    }

    render() {
        if (isDebug) {
            this.game.debug.cameraInfo(this.game.camera, 500, 32);
            this.game.debug.spriteCoords(GameState.player.tank, 32, 32);
        }
    }
}
