import { State } from "phaser-ce";

export class BootState extends State {
    create() {
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;               // Shows the entire game while maintaining proportions
        this.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.parentIsWindow = true;
        this.game.state.start('game');
    }
}
