import { Game, Sprite, Weapon } from "phaser-ce";
import { GameState } from "state/GameState";
import { isDebug, TankMapping } from "system/config";
import { TankBullet } from "./TankBullet";

const INIT_FIRE_ANGLE = 270;
/**
 * Tank can be controlled by player
 *
 * @export
 * @class Tank
 * @extends {Sprite}
 */
export class Tank extends Sprite {

    public weapon: Weapon;

    public power = 10;

    private fireFrom: number;

    private speed = 100;

    public isSelf = true;
    constructor(game: Game, x: number, y: number, config: TankMapping) {
        super(game, x, y, config.texture);
        this.name = 'tank';

        this.game.physics.p2.enable(this, isDebug);
        const wgroup = this.game.add.group();
        wgroup.name = 'bullet_pool';
        this.weapon = game.add.weapon(-1, 'bullet', null, wgroup, TankBullet as any);
        this.weapon.bulletKillType = Weapon.KILL_LIFESPAN;

        this.weapon.fireFrom.setTo(x + 8, y - 80, 1, 1);
        this.weapon.fireAngle = INIT_FIRE_ANGLE;
        this.weapon.bulletLifespan = 2000;
        this.fireFrom = this.height * 0.7;

        this.updateTankState(config);

        this.game.camera.follow(this);
    }

    /**
     * body.angle range: -180 ~ 180, normalize the body angle to degree 0~360
     *
     * @private
     * @returns
     * @memberof Tank
     */
    private normalizeAngle() {
        const angle = Math.floor(this.body.angle);

        if (angle > 0) {
            if (angle <= 90) {
                return 90 - angle
            } else {
                return 360 - (angle - 90)
            }
        } else if (angle < 0) {
            return Math.abs(angle) + 90;
        } else {
            return 90;
        }
    }
    /**
     * update tank state by config
     *
     * @param {TankMapping} config
     * @memberof Tank
     */
    updateTankState(config: TankMapping) {
        this.loadTexture(config.texture);
        this.weapon.bulletSpeed = config.bulletSpeed;
        this.setFireRate(config.fireRate);
        this.setSpeed(config.speed);
        this.power = config.damage;

    }

    /**
     * Set the tank fire rate ( Bullets per second )
     *
     * @param {number} bulletPerSec
     * @memberof Tank
     */
    setFireRate(bulletPerSec: number) {
        this.weapon.fireRate = 1000 / bulletPerSec;
    }

    /**
     * Set the tank moving speed
     *
     * @param {number} bulletPerSec
     * @memberof Tank
     */
    setSpeed(speed: number) {
        this.speed = speed;
    }

    update() {
        const body = (this.body as Phaser.Physics.P2.Body);
        body.setZeroVelocity();
        body.angularVelocity = 0;

        // only self play is allow control
        if (this.isSelf) {
            if (GameState.controlPanel.isFireKeyPress) {
                this.weapon.fireAngle = INIT_FIRE_ANGLE + this.body.angle;
                this.weapon.x = this.x + this.fireFrom * Math.cos(this.weapon.fireAngle * Math.PI / 180)
                this.weapon.y = this.y + this.fireFrom * Math.sin(this.weapon.fireAngle * Math.PI / 180)
                this.weapon.fire();
            }

            if (GameState.controlPanel.isLeftKeyPress) {
                this.body.angle -= 1;
            } else if (GameState.controlPanel.isRightKeyPress) {
                this.body.angle += 1;
            }
            const angle = this.normalizeAngle();
            if (GameState.controlPanel.isUpKeyPress) {
                this.body.moveUp(this.speed * Math.sin(angle * Math.PI / 180));
                this.body.moveRight(this.speed * Math.cos(angle * Math.PI / 180));
            } else if (GameState.controlPanel.isDownKeyPress) {
                this.body.moveUp(-this.speed * Math.sin(angle * Math.PI / 180));
                this.body.moveRight(-this.speed * Math.cos(angle * Math.PI / 180));
            }
        }
    }
}
