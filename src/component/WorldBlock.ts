import { Game, Group } from "phaser-ce";
import { obstacleConfig } from "system/config";
import { ObstacleDef } from "system/enum";
import { gameStore } from "system/GameStore";
import { MapGenerator, Point } from "system/interface";
import { getCenterByBlockIndex, random } from "util/util";
import { Obstacle } from "./Obstacle";

const OBSTACLE_NUM = 5;
/**
 * Create map block
 *
 * @export
 * @class WorldBlock
 * @extends {Group}
 * @implements {MapGenerator}
 */
export class WorldBlock extends Group implements MapGenerator {

    public center: Point;
    public blockIndex: Point;
    public table: number[] = [];
    constructor(game: Game, x: number, y: number, resourceTable: number[] = []) {
        super(game);

        this.blockIndex = { x, y };
        this.table = resourceTable;
        this.center = getCenterByBlockIndex(x, y);
        this.name = `${x}_${y}`;
        console.log(`create block (${x},${y})`);
        this.generate();
        gameStore.createdBlocks[this.name] = this;

    }

    private getObstacleType = (seed: number) => {
        let idx = 0;
        while (this.table[idx] < seed) {
            idx++;
        }
        return idx;
    }

    private generateObstacle(type: ObstacleDef) {
        let obstacle: Obstacle;

        const x = this.center.x + random(-this.game.width / 2, this.game.width / 2);
        const y = this.center.y + random(-this.game.height / 2, this.game.height / 2);

        switch (type) {
            case ObstacleDef.HAY:
                obstacle = new Obstacle(this.game, x, y, 'hay', obstacleConfig[ObstacleDef.HAY]);
                break;
            case ObstacleDef.WALL:
                obstacle = new Obstacle(this.game, x, y, 'wall', obstacleConfig[ObstacleDef.WALL]);
                break;
        }
        this.add(obstacle);
    }

    generate() {
        if (this.table.length === 0) {
            console.error('no table definition.');
            return;
        }

        for (let i = 1; i <= OBSTACLE_NUM; i++) {
            const seed = random(0, 10);
            const type = this.getObstacleType(seed);
            this.generateObstacle(type);
        }
    }
}
