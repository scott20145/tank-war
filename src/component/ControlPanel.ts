import { Game, Group, Key } from "phaser-ce";
import { GameState } from "state/GameState";
import { tankConfig } from "system/config";
import { TankDef } from "system/enum";
import { gameStore } from "system/GameStore";
/**
 * Create control Panel UI and relational input control
 *
 * @export
 * @class ControlPanel
 * @extends {Group}
 */
export class ControlPanel extends Group {
    public isUpKeyPress: boolean = false;
    public isDownKeyPress: boolean = false;
    public isLeftKeyPress: boolean = false;
    public isRightKeyPress: boolean = false;
    public isFireKeyPress: boolean = false;

    private keyA: Key;
    private keyD: Key;
    private keyW: Key;
    private keyS: Key;
    private space: Key;

    private mousePress = false;

    constructor(game: Game, x = 0, y = 0) {
        super(game);
        this.createBtn(50, 150, 'A', () => { this.mousePress = true; this.isLeftKeyPress = true }, () => { this.mousePress = false; this.isLeftKeyPress = false });
        this.createBtn(150, 50, 'W', () => { this.mousePress = true; this.isUpKeyPress = true }, () => { this.mousePress = false; this.isUpKeyPress = false });
        this.createBtn(250, 150, 'D', () => { this.mousePress = true; this.isRightKeyPress = true }, () => { this.mousePress = false; this.isRightKeyPress = false });
        this.createBtn(150, 150, 'S', () => { this.mousePress = true; this.isDownKeyPress = true }, () => { this.mousePress = false; this.isDownKeyPress = false });

        this.createBtn(50, 250, 'T1', () => {
            gameStore.player.tank = tankConfig[TankDef.RED]
            GameState.player.tank.updateTankState(gameStore.player.tank);
        });
        this.createBtn(150, 250, 'T2', () => {
            gameStore.player.tank = tankConfig[TankDef.BLUE]
            GameState.player.tank.updateTankState(gameStore.player.tank);
        });
        this.createBtn(250, 250, 'T3', () => {
            gameStore.player.tank = tankConfig[TankDef.GREEN]
            GameState.player.tank.updateTankState(gameStore.player.tank);
        });

        this.keyA = this.game.input.keyboard.addKey(Phaser.Keyboard.A);
        this.keyD = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
        this.keyW = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
        this.keyS = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
        this.space = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        this.x = x;
        this.y = y;
        this.fixedToCamera = true;
    }

    private createBtn(x: number, y: number, str: string, onKeyDown: Function = () => { }, onKeyUp: Function = () => { }) {
        const group = this.game.add.group();
        group.inputEnableChildren = true;
        const bg = this.game.add.graphics(0, 0);
        bg.beginFill(0xffffff, .7);
        bg.drawCircle(x, y, 76);
        bg.endFill();
        const style = { font: "Microsoft JhengHei", fill: "#6970FF", fontSize: 54 };

        const text = this.game.add.text(x, y, str, style, this);
        text.alpha = .5;
        text.anchor.setTo(.5);
        group.add(bg);
        group.add(text);
        group.onChildInputOver.add(() => {
            this.game.canvas.style.cursor = 'pointer'
        })
        group.onChildInputOut.add(() => {
            this.game.canvas.style.cursor = 'default'
        })

        group.onChildInputDown.add(onKeyDown);
        group.onChildInputUp.add(onKeyUp);

        this.add(group);
    }

    update() {
        // if no mouse input, then check keyboard input
        if (!this.mousePress) {
            this.isLeftKeyPress = this.keyA.isDown;
            this.isRightKeyPress = this.keyD.isDown;
            this.isUpKeyPress = this.keyW.isDown;
            this.isDownKeyPress = this.keyS.isDown;
            this.isFireKeyPress = this.space.isDown;
        }
    }
}
