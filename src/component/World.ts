import { Game, Group, Sprite } from "phaser-ce";
import { GameState } from "state/GameState";
import { obstacleTable } from "system/config";
import { Direction } from "system/enum";
import { gameStore } from "system/GameStore";
import { Bound, Point } from "system/interface";
import { getBlockIndexByPosition, getBoundByBlockIndex } from "util/util";
import { Obstacle } from "./Obstacle";
import { WorldBlock } from "./WorldBlock";

const NEAR_BOUND_DISTANCE = 300;
/**
 * Create game world which consist of many WorldBlocks.
 *
 * @export
 * @class World
 * @extends {Group}
 */
export class World extends Group {
    private currentBound: Bound;

    public readonly bornPoint: Point;

    public table: number[] = [];

    public currentBlock: WorldBlock;
    private prevBlockIndex: Point;
    private prevPlayerPoint: Point;
    /**
     * Creates an instance of World.
     * @param {Game} game
     * @param {number} x world block X Axis Index
     * @param {number} y world block Y Axis Index
     * @memberof World
     */
    constructor(game: Game, x: number, y: number) {
        super(game);
        this.name = 'world';
        this.bornPoint = { x, y };

        this.table = obstacleTable;
        const block = new WorldBlock(game, x, y, this.table);
        this.currentBlock = block;
        this.prevBlockIndex = block.blockIndex;
        this.add(block);
        this.createAdjacentBlock(x, y);
        this.currentBound = getBoundByBlockIndex(x, y);
    }

    private createAdjacentBlock(x: number, y: number) {
        this.add(new WorldBlock(this.game, x - 1, y - 1, this.table));
        this.add(new WorldBlock(this.game, x, y - 1, this.table));
        this.add(new WorldBlock(this.game, x + 1, y - 1, this.table));
        this.add(new WorldBlock(this.game, x - 1, y, this.table));
        this.add(new WorldBlock(this.game, x + 1, y, this.table));
        this.add(new WorldBlock(this.game, x - 1, y + 1, this.table));
        this.add(new WorldBlock(this.game, x, y + 1, this.table));
        this.add(new WorldBlock(this.game, x + 1, y + 1, this.table));
    }

    private createBlock(x: number, y: number) {
        if (x < 0 || y < 0) {
            return;
        }

        if (!gameStore.createdBlocks[`${x}_${y}`]) {
            this.add(new WorldBlock(this.game, x, y, this.table));
        } else if (!this.getByName(`${x}_${y}`)) {
            // console.log(`reload (${x},${y})`);
            this.add(gameStore.createdBlocks[`${x}_${y}`]);
        }
    }

    private getBound(obj: Sprite): { up: number, right: number, down: number, left: number } {
        const x = obj.x;
        const y = obj.y;
        return {
            up: y,
            right: x + obj.width,
            down: y + obj.height,
            left: x
        }
    }

    private unmountBlock() {
        const curBlockIdx = this.currentBlock.blockIndex;
        let removed: Point[] = [];
        if (curBlockIdx.y < this.prevBlockIndex.y) { // move top
            removed = [
                { x: curBlockIdx.x - 1, y: curBlockIdx.y + 2 },
                { x: curBlockIdx.x, y: curBlockIdx.y + 2 },
                { x: curBlockIdx.x + 1, y: curBlockIdx.y + 2 },
            ]

        } else if (curBlockIdx.y > this.prevBlockIndex.y) { // move down
            removed = [
                { x: curBlockIdx.x - 1, y: curBlockIdx.y - 2 },
                { x: curBlockIdx.x, y: curBlockIdx.y - 2 },
                { x: curBlockIdx.x + 1, y: curBlockIdx.y - 2 }
            ]
        } else if (curBlockIdx.x < this.prevBlockIndex.x) { // move left
            removed = [
                { x: curBlockIdx.x + 2, y: curBlockIdx.y - 1 },
                { x: curBlockIdx.x + 2, y: curBlockIdx.y },
                { x: curBlockIdx.x + 2, y: curBlockIdx.y + 1 }
            ]
        } else { // move right;
            removed = [
                { x: curBlockIdx.x - 2, y: curBlockIdx.y - 1 },
                { x: curBlockIdx.x - 2, y: curBlockIdx.y },
                { x: curBlockIdx.x - 2, y: curBlockIdx.y + 1 }
            ]
        }

        removed.forEach((p: Point) => {
            if (p.x < 0 || p.y < 0) {
                return;
            }
            const block = this.getByName(`${p.x}_${p.y}`);
            // console.log(`unmount (${p.x},${p.y})`);
            this.removeChild(block);
        })
    }

    private preCreateBlock(direction: Direction) {
        const blockIdx = this.currentBlock.blockIndex;
        switch (direction) {
            case Direction.DOWN:
                this.createBlock(blockIdx.x - 1, blockIdx.y + 2);
                this.createBlock(blockIdx.x, blockIdx.y + 2);
                this.createBlock(blockIdx.x + 1, blockIdx.y + 2);
                break;
            case Direction.UP:
                this.createBlock(blockIdx.x - 1, blockIdx.y - 2);
                this.createBlock(blockIdx.x, blockIdx.y - 2);
                this.createBlock(blockIdx.x + 1, blockIdx.y - 2);
                break;
            case Direction.LEFT:
                this.createBlock(blockIdx.x - 2, blockIdx.y - 1);
                this.createBlock(blockIdx.x - 2, blockIdx.y);
                this.createBlock(blockIdx.x - 2, blockIdx.y + 1);
                break;
            case Direction.RIGHT:
                this.createBlock(blockIdx.x + 2, blockIdx.y - 1);
                this.createBlock(blockIdx.x + 2, blockIdx.y);
                this.createBlock(blockIdx.x + 2, blockIdx.y + 1);
                break;
        }
    }

    overlap(obj: Sprite, overlapCallback: (sprite: Sprite) => void) {
        this.children.forEach((block: WorldBlock) => {
            block.children.forEach((item: Obstacle) => {
                const bound = this.getBound(item);
                if (item.alive && obj.x >= bound.left && obj.x <= bound.right && obj.y >= bound.up && obj.y <= bound.down) {

                    if (item.canKill) {
                        item.damage((obj as any).power)
                        item.isCollide = true;
                    }
                    overlapCallback(item);
                }
            })
        });
    }

    update() {
        super.update();
        const position = GameState.player.getPosition();

        // need preload block or not
        if (Math.abs(position.y - this.currentBound.down) < NEAR_BOUND_DISTANCE && this.prevPlayerPoint.y < position.y) {
            this.preCreateBlock(Direction.DOWN);
        } else if (Math.abs(position.y - this.currentBound.up) < NEAR_BOUND_DISTANCE && this.prevPlayerPoint.y > position.y) {
            this.preCreateBlock(Direction.UP);
        }
        if (Math.abs(position.x - this.currentBound.left) < NEAR_BOUND_DISTANCE && this.prevPlayerPoint.x > position.x) {
            this.preCreateBlock(Direction.LEFT);
        } else if (Math.abs(position.x - this.currentBound.right) < NEAR_BOUND_DISTANCE && this.prevPlayerPoint.x < position.x) {
            this.preCreateBlock(Direction.RIGHT);
        }

        if (this.currentBound.left > position.x ||
            this.currentBound.right < position.x ||
            this.currentBound.up > position.y ||
            this.currentBound.down < position.y
        ) {
            const blockIndex = getBlockIndexByPosition(position.x, position.y);
            this.currentBound = getBoundByBlockIndex(blockIndex.x, blockIndex.y);
            this.prevBlockIndex = this.currentBlock.blockIndex;
            this.currentBlock = gameStore.createdBlocks[`${blockIndex.x}_${blockIndex.y}`]
            console.log(`(${this.prevBlockIndex.x},${this.prevBlockIndex.y}) to (${blockIndex.x},${blockIndex.y})`);
            this.unmountBlock();
        }

        this.prevPlayerPoint = position;
    }
}
