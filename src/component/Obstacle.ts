import { Game, Sprite } from "phaser-ce";
import { isDebug, ObstacleMapping } from "system/config";
/**
 * Create world obstacle
 *
 * @export
 * @class Obstacle
 * @extends {Sprite}
 */
export class Obstacle extends Sprite {
    public hasBody: boolean;
    public cursors: Phaser.CursorKeys;

    public canKill = false;

    public isCollide = false;

    private collideLoop = 0;
    constructor(game: Game, x: number, y: number, key: string, config: ObstacleMapping) {
        super(game, x, y, key)

        this.scale.setTo(config.scale);
        this.health = config.health;
        this.canKill = config.cankill;

        if (config.hasBody) {
            this.game.physics.p2.enable(this, isDebug);
            const body = (this.body as Phaser.Physics.P2.Body);
            body.static = true;
        }
    }

    update() {
        if (this.isCollide) {
            this.tint = 0xf44a6c;
            this.collideLoop = 1;
        }

        if (this.collideLoop === 10) {
            this.tint = 0xffffff;
            this.collideLoop = 0;
        } else if (this.collideLoop !== 0) {
            this.collideLoop++;
        }
        this.isCollide = false;
    }
}
