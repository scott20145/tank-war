import { Bullet, Sprite } from "phaser-ce";
import { GameState } from "state/GameState";

/**
 * Overrides original Bullet class to handle hit objects.
 *
 * @export
 * @class TankBullet
 * @extends {Bullet}
 */
export class TankBullet extends Bullet {
    public power: number;
    constructor(game: Phaser.Game, x: number, y: number, key?: any, frame?: any) {

        super(game, x, y, key, frame);
    }
    update() {
        if (this.exists && this.alive) {
            this.power = GameState.player.tank.power;
            GameState.world.overlap(this, this.onOverlap.bind(this));
            this.checkReachBound();
        }
    }

    checkReachBound() {
        const bound = this.game.world.bounds;
        if (this.x < 0 || this.x > bound.width || this.y < 0 || this.y > bound.height) {
            this.kill();
        }
    }

    onOverlap(_target: Sprite) {
        this.kill();
    }
}
