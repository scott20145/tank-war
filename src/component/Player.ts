import { Game } from "phaser-ce";
import { tankConfig } from "system/config";
import { TankDef } from "system/enum";
import { gameStore } from "system/GameStore";
import { Point } from "system/interface";
import { Tank } from "./Tank";
/**
 * A Player class includes tank, id,etc.
 *
 * @export
 * @class Player
 */
export class Player {
    public tank: Tank;
    public playerId: string;

    constructor(game: Game, x: number, y: number, isSelf = true) {
        let tconfig;
        if (isSelf) {
            tconfig = tankConfig[TankDef.RED];
            gameStore.player.tank = tconfig;
            this.tank = new Tank(game, x, y, tconfig);
        } else {
            throw new Error('other play not implemented!')
        }
        game.world.add(this.tank);
    }

    getPosition(): Point {
        return {
            x: this.tank.x,
            y: this.tank.y
        }
    }
}
