# Tank-War

### Introduction

Tank-War is a simple prototype using [Phaser](https://github.com/photonstorm/phaser-ce).

### Install

```
npm install
```

### Command

start local env：
```
npm run start
```

build for production：
```
npm run build
```

### Control

Please use the control panel on the screen to control tank.

#### Control Panel:
`A`: left rotation
`D`: right rotation
`W`: move forward
`S`: move backward
`T1`,`T2`,`T3`: switch tank type

#### keyboard control:
`A`: left rotation
`D`: right rotation
`W`: move forward
`S`: move backward
`SPACE BAR`: fire bullet

### Layer

- ROOT
    - Game World
    - Player
    - Control Panel

### Map 
Game World consist of Game Blocks. The Game World's height/width is `MAX_INTEGER` which is nearly like unlimited world.
The width and height of a Game Block is defined by `game.width` and `game.height`. Default is `1920`x`1080`

example:  


| (0,0) | (0,1) | (0,2) |
| -------- | -------- | -------- |
| (1,0) | (1,1)* | (1,2) |
| (2,0) | (2,1) | (2,2) |

The user is created at block`(1,1)`, then all adjacent blocks will be created. Additional blocks will be created when the player near the current blocks bound.
 